// Khai báo thư viện express
const express = require('express');
const path = require('path');
// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo app
const app = express();

// Khai báo port
const port = 8000;

// Call api run project course365
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname + "/views/index.html"));
})
//hiển thị hình ảnh cần thêm  middleware static vào express
app.use(express.static(__dirname +"/views"));

// Gọi review Model
const reviewModel = require("./app/models/reviewModel");
const courseModel = require("./app/models/courseModel");

// Import Router
const courseRouter = require('./app/routes/courseRouter');
const reviewRouter = require('./app/routes/reviewRouter');

// Cấu hình request đọc được body json
app.use(express.json());

app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method: ", request.method);
    next();
}
)

// Connect MongoDB
main().catch(err => console.log(err));
async function main() {
    await mongoose.connect('mongodb://127.0.0.1/CourseDatabase');
    console.log("Connect MongoDB Successfully");
}

app.use('/api', courseRouter)
app.use('/api', reviewRouter)

// Khởi động app
app.listen(port, () => {
    console.log("App listening on port: ", port);
})