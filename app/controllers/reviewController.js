// 
const mongoose = require('mongoose');

// 
const reviewModel = require('../models/reviewModel');
const courseModel = require('../models/courseModel');

//
const createReviewOfCourse = (request, response) => {
    let courseId = request.params.courseId;
    let requestBody = request.body;

    // Validate
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: 'Bad request',
            message: 'Course ID is invalid'
        })
    }
    if (!(Number.isInteger(requestBody.rate) && requestBody.rate > 0 && requestBody.rate <= 5)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Rate is invalid'
        })
    }

    let newReview = {
        _id: mongoose.Types.ObjectId(),
        stars: requestBody.rate,
        note: requestBody.note
    }

    reviewModel.create(newReview, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        }
        else {
            courseModel.findByIdAndUpdate(courseId,
                {
                    $push: { reviews: data._id }
                }, (err, updatedCourse) => {
                    if (err) {
                        return response.status(500).json({
                            status: 'Internal server error',
                            message: err.message
                        })
                    } else {
                        return response.status(201).json({
                            status: 'Create Review Success',
                            data: data
                        })
                    }
                }
            )
        }
    })
}

//
const getAllReviewOfCourse = (request, response) => {
    let courseId = request.params.courseId;

    // Validate
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Course ID is invalid'
        })
    }
    //
    courseModel.findById(courseId)
        .populate('reviews')
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: 'Internal server error',
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: 'Get data success',
                    data: data.reviews
                })
            }
        })
}

//
const getReviewById = (request, response) => {
    let reviewId = request.params.reviewId;

    // Validate
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Review ID is invalid'
        })
    }
    reviewModel.findById(reviewId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Success: Get review success',
                data: data
            })
        }
    })
}

// update Review By Id
const updateReviewById = (request, response) => {
    let reviewId = request.params.reviewId;
    let bodyRequest = request.body;

    // Validate
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Review ID is invalid'
        })
    }

    let updateReview = {
        stars: bodyRequest.rate,
        node: bodyRequest.note
    }

    reviewModel.findByIdAndUpdate(reviewId, updateReview, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: 'Success: Update review success',
                data: data
            })
        }
    })
}

// Delete
const deleteReviewById = (request, response) => {
    let courseId = request.params.courseId;
    let reviewId = request.params.reviewId;
    // Validate
    if(!mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Course ID is invalid'
        })
    }
    if(!mongoose.Types.ObjectId.isValid(reviewId)) {
        return response.status(400).json({
            status: 'Bad Request',
            message: 'Review ID is invalid'
        })
    }
    reviewModel.findByIdAndDelete(reviewId, (error) => {
        if(error) {
            return response.status(500).json({
                status: 'Internal server error',
                message: error.message
            })
        } else {
            courseModel.findByIdAndUpdate(courseId, 
            {
                $pull: { reviews: reviewId }
            },
            (err, updatedCourse) => {
                if(err) {
                    return response.status(500).json({
                        status: 'Internal server error',
                        message: err.message
                    })
                } else {
                    return response.status(204).json({
                        status: 'Success: Delete review success'
                    })
                }
            }
            )
        }
    })
}

module.exports = {
    createReviewOfCourse,
    getAllReviewOfCourse,
    getReviewById,
    updateReviewById,
    deleteReviewById
}