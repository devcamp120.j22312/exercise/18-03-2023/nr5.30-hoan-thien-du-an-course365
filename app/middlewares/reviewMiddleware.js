const getAllReviewMiddleware = (req, res, next) => {
    console.log("Get All Review");
    next();
}

const getReviewMiddleware = (req, res, next) => {
    console.log("Get Review");
    next();
}

const postReviewMiddleware = (req, res, next) => {
    console.log("Create a Review");
    next();
}

const putReviewMiddleware = (req, res, next) => {
    console.log("Update a Review");
    next();
}

const deleteReviewMiddleware = (req, res, next) => {
    console.log("Delete Review");
    next();
}

module.exports = {
    getAllReviewMiddleware,
    getReviewMiddleware,
    postReviewMiddleware,
    putReviewMiddleware,
    deleteReviewMiddleware
}