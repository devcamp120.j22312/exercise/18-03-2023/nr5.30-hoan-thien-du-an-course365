// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo course Schema
const courseSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
    },
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
    reviews: [ 
        {
            type: mongoose.Types.ObjectId,
            ref: "review"
        }
    ]

});

// Biên dịch review Model từ ReviewSChema
module.exports = mongoose.model("Course", courseSchema);