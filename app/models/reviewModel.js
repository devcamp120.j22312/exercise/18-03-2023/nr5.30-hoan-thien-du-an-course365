// Import thư viện mongoose
const mongoose = require('mongoose');

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo review Schema
const reviewSchema = new Schema ({
    _id: {
        type: mongoose.Types.ObjectId
    }, 
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    }
});

// Biên dịch review Model từ ReviewSChema
module.exports = mongoose.model("Review", reviewSchema);